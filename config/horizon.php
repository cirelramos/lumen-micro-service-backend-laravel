<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Horizon Redis Connection
    |--------------------------------------------------------------------------
    |
    | This is the name of the Redis connection where Horizon will store the
    | meta information required for it to function. It includes the list
    | of supervisors, failed jobs, job metrics, and other information.
    |
    */

    'use' => 'default',


    /*
    |--------------------------------------------------------------------------
    | Horizon Redis Prefix
    |--------------------------------------------------------------------------
    |
    | This prefix will be used when storing all Horizon data in Redis. You
    | may modify the prefix when you are running multiple installations
    | of Horizon on the same server so that they don't have problems.
    |
    */

    'prefix' => env('HORIZON_PREFIX', 'horizon:'),

    /*
    |--------------------------------------------------------------------------
    | Horizon Route Middleware
    |--------------------------------------------------------------------------
    |
    | These middleware will get attached onto each Horizon route, giving you
    | the chance to add your own middleware to this list or change any of
    | the existing middleware. Or, you can simply stick with this list.
    |
    */

    'middleware' => ['web'],

    /*
    |--------------------------------------------------------------------------
    | Queue Wait Time Thresholds
    |--------------------------------------------------------------------------
    |
    | This option allows you to configure when the LongWaitDetected event
    | will be fired. Every connection / queue combination may have its
    | own, unique threshold (in seconds) before this event is fired.
    |
    */

    'waits' => [
        'redis:default' => 180,
    ],

    /*
    |--------------------------------------------------------------------------
    | Job Trimming Times
    |--------------------------------------------------------------------------
    |
    | Here you can configure for how long (in minutes) you desire Horizon to
    | persist the recent and failed jobs. Typically, recent jobs are kept
    | for one hour while all failed jobs are stored for an entire week.
    |
    */

    'trim' => [
        'recent' => 180,
        'failed' => 10080,
    ],

    /*
    |--------------------------------------------------------------------------
    | Fast Termination
    |--------------------------------------------------------------------------
    |
    | When this option is enabled, Horizon's "terminate" command will not
    | wait on all of the workers to terminate unless the --wait option
    | is provided. Fast termination can shorten deployment delay by
    | allowing a new instance of Horizon to start while the last
    | instance will continue to terminate each of its workers.
    |
    */

    'fast_termination' => false,

    /*
    |--------------------------------------------------------------------------
    | Queue Worker Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may define the queue worker settings used by your application
    | in all environments. These supervisors and settings handle all your
    | queued jobs and will be provisioned by Horizon during deployment.
    |
    */

    'environments' => [
        'production' => [
            'supervisor-prod' => [
                'connection' => 'redis-long-running',
                'queue' => ['notifications', 'emails', 'imports', 'exports', 'default'],
                'maxProcesses' => 30,
                'minProcesses' => 5,
                'delay' => 2,
                'memory' => 4000,
                'timeout' => 7200,
                'sleep' => 3,
                'maxTries' => 1,
                'balance' => 'auto', // could be simple, auto, or null
                'force' => false,
            ],
            'supervisor-long-prod' => [
                'connection' => 'redis-long-running',
                'queue' => ['processes'],
                'maxProcesses' => 30,
                'minProcesses' => 2,
                'delay' => 2,
                'memory' => 4000,
                'timeout' => 7200,
                'sleep' => 3,
                'maxTries' => 1,
                'balance' => 'auto', // could be simple, auto, or null
                'force' => false,
            ],
        ],

        'local' => [
            'supervisor-local' => [
                'connection' => 'redis',
                'queue' => ['notifications', 'emails', 'imports', 'exports', 'default'],
                'maxProcesses' => 4,
                'minProcesses' => 4,
                'delay' => 2,
                'memory' => 768,
                'timeout' => 750,
                'sleep' => 3,
                'tries' => 2,
                'maxTries' => 3,
                'balance' => 'auto', // could be simple, auto, or null
                'force' => false,
            ],

            'supervisor-long-local' => [
                'connection' => 'redis-long-running',
                'queue' => ['processes'],
                'maxProcesses' => 2,
                'minProcesses' => 1,
                'delay' => 0,
                'memory' => 2048,
                'timeout' => 5200,
                'sleep' => 3,
                'tries' => 1,
                'maxTries' => 2,
                'balance' => 'simple', // could be simple, auto, or null
                'force' => false,
            ],
        ],
    ],
];
