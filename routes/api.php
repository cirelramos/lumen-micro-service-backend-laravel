<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('oauth/login', 'Auth\LoginController@login')->name('login');
Route::get('oauth/logout', 'Auth\LoginController@logout')->name('logout');
Route::resource('users','User\UserController');
Route::post( 'groups/{group}/associate/user/', 'Group\GroupController@associateUser' )->name( 'groups.associate_user' );
Route::resource('groups','Group\GroupController');
Route::resource('notes','Note\NoteController');
