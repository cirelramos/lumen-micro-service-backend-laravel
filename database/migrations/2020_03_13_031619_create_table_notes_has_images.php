<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotesHasImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes_has_images', function (Blueprint $table) {
            $table->integer('id_image')->unsigned()->comment('relation images');
            $table->integer('id_note')->unsigned()->comment('relation notes');
            $table->foreign('id_image')
                ->references('id_image')
                ->on('images')
                ->onDelete('cascade')
            ;
            $table->foreign('id_note')
                ->references('id_note')
                ->on('notes')
                ->onDelete('cascade')
            ;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes_has_images');
    }
}
