<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

/**
 * Trait CacheRedisTraits
 *
 * @package App\Traits
 */
trait CacheRedisTraits
{
    /**
     * Extract the key saved in redis
     *
     * @param string $key      name of the key
     * @param string $userId   id of user
     * @param        $company
     * @param string $database name of the database redis
     * @return mixed
     */
    protected function getCache($key, $userId = '', $company = '', $database = '')
    {
        $keyCustoms = $this->generateKeyCacheAuth($key, $userId, $company);

        if (!empty($database)) {
            $response = Cache::store($database)->get($keyCustoms);
        } else {
            $response = Cache::get($keyCustoms);
        }

        return $response;
    }

    /**
     * Save or update the key in redis
     *
     * @param string $key      name of the key
     * @param mixed  $values   the object to be saved
     * @param int    $time     time in session
     * @param string $userId   id of user
     * @param        $company
     * @param string $database name of the database redis
     */
    protected function cache($key, $values, $time, $userId = '', $company = '', $database = ''): void
    {
        $keyCustoms = $this->generateKeyCacheAuth($key, $userId, $company);

        if (!empty($database)) {
            Cache::store($database)->put($keyCustoms, $values, $time);
        } else {
            Cache::put($keyCustoms, $values, $time);
        }
    }

    /**
     * Validate if your key exists in redis
     *
     * @param string $key      name of the key
     * @param string $userId   id of user
     * @param        $company
     * @param string $database name of the database redis
     * @return bool
     */
    protected function hasCache($key, $userId = '', $company = '', $database = ''): bool
    {
        $keyCustoms = $this->generateKeyCacheAuth($key, $userId, $company);
        if (!empty($database)) {
            if (!empty(Cache::store($database)->has($keyCustoms))) {
                $status = true;
            } else {
                $status = false;
            }
        } elseif (empty(Cache::has($keyCustoms))) {
            $status = false;
        } else {
            $status = true;
        }

        return $status;
    }

    /**
     * Delete the key saved in redis
     *
     * @param string $key      name of the key
     * @param string $userId   id of user
     * @param        $company
     * @param string $database name of the database redis
     * @return void
     */
    protected function forgetCache($key, $userId = '', $company = '', $database = ''): void
    {
        $keyCustoms = $this->generateKeyCacheAuth($key, $userId, $company);

        if (!empty($database)) {
            Cache::store($database)->forget($keyCustoms);
        } else {
            Cache::forget($keyCustoms);
        }
    }

    /**
     * Initiating the progress in 1%
     *
     * @param $name
     * @param $time
     * @param $user
     * @param $company
     */
    protected function cacheProgressStarting($name, $time, $user, $company): void
    {
        $response = ['percentage' => 1, 'status' => 'Starting', 'success' => true];
        $this->cache($name, $response, $time, $user, $company);
    }

    /**
     * Process progress in redis
     *
     * @param       $name
     * @param       $time
     * @param       $user
     * @param       $company
     * @param       $progress
     * @param array $params
     */
    protected function cacheProgressProcessing($name, $time, $user, $company, $progress, $params = []): void
    {
        if (!empty($params)) {
            $response = array_merge(['percentage' => ceil($progress), 'status' => 'Processing', 'success' => true],
                $params);
        } else {
            $response = ['percentage' => ceil($progress), 'status' => 'Processing', 'success' => true];
        }

        $this->cache($name, $response, $time, $user, $company);
    }

    /**
     * Complete process in 100%
     *
     * @param       $name
     * @param       $time
     * @param       $user
     * @param       $company
     * @param array $params
     * @return void
     */
    protected function cacheProgressComplete($name, $time, $user, $company, $params = []): void
    {
        if (!empty($params)) {
            $response = array_merge(['percentage' => 100, 'status' => 'Complete', 'success' => true], $params);
        } else {
            $response = ['percentage' => 100, 'status' => 'Complete', 'success' => true];
        }

        $this->cache($name, $response, $time, $user, $company);
    }

    /**
     * Execute a failed process
     *
     * @param       $name
     * @param       $time
     * @param       $user
     * @param       $company
     * @param array $params
     */
    protected function cacheProgressFails($name, $time, $user, $company, $params = []): void
    {
        if (!empty($params)) {
            $response = array_merge(['percentage' => 0, 'status' => 'Fails', 'success' => false], $params);
        } else {
            $response = ['percentage' => 0, 'status' => 'Fails', 'success' => false];
        }

        $this->cache($name, $response, $time, $user, $company);
    }

    /**
     * Delete the progress after complete
     *
     * @param $name
     * @param $user
     * @param $company
     */
    protected function cacheProgressForget($name, $user, $company): void
    {
        $this->forgetCache($name, $user, $company);
    }

    /**
     * Returns the status of the process
     *
     * @param       $identify
     * @param       $user
     * @param       $company
     * @param array $params
     * @return JsonResponse
     */
    protected function statusProgressJobs($identify, $user, $company, $params = []): JsonResponse
    {
        $responseCache = $this->getCache($identify, $user, $company);
        if ($responseCache !== null) {
            $data = $this->responseValue($responseCache, $params);
            switch ($data) {
                case ((((int)$data['percentage'] < 100) && ((int)$data['percentage'] > 0)) && (boolean)$data['success'] === true):
                    $response = $this->successResponse([$data], 206);
                    $flushCache = false;
                    break;
                case ((int)$data['percentage'] === 100 && (boolean)$data['success'] === true):
                    $response = $this->successResponse([$data]);
                    $flushCache = true;
                    break;
                case ((int)$data['percentage'] === 0 && (boolean)$data['success'] === false):
                    $response = $this->successResponse([$data], 404);
                    $flushCache = true;
                    break;
                default:
                    $response = $this->successResponse([$data], 400);
                    $flushCache = true;
                    break;
            }

            if ($flushCache) {
                /** elimina todos las respuestas despues del 100% **/
                $this->forgetCache($identify, $user, $company);
            }
        } else {

            $response = $this->errorResponse(['status' => 'No data', 'success' => false], 400);
        }

        return $response;
    }

    /**
     * valid information if you need a parameter
     *
     * @param $response
     * @param $params
     * @return array
     */
    private function responseValue($response, $params): array
    {
        if (empty($params)) {
            $responseData = $response;
        } else {
            $responseData = array_merge($response, $params);
        }

        return $responseData;
    }

    /**
     * Generate identify dynamic in the redis
     *
     * @param        $key
     * @param string $userId
     * @param string $company
     * @return string
     */
    private function generateKeyCacheAuth($key, $userId = '', $company = ''): string
    {
        return str_slug($key . '-' . md5($userId . ' - ' . $company),'_');
    }

    /**
     * @param $key
     * @return array
     */
    protected function getKeysByPattern( $key )
    {
        $response = Redis::connection( 'default' )->keys( $key );
        return $response;
    }

    protected function keyPatternTransform($key){
        $newKey  = null;
        $user    = null;
        $company = null;
        $explode_key = explode( ':', $key );
        if ( array_key_exists( 1, $explode_key ) ) {
            $explode_key   = explode( '_', $explode_key[ 1 ] );
            $explode_count = count( $explode_key );
            $user          = $explode_key[ 0 ];
            $company       = $explode_key[ 1 ];
            unset( $explode_key[ $explode_count - 1 ] );
            unset( $explode_key[ 0 ] );
            unset( $explode_key[ 1 ] );
            $newKey = $user . '-' . $company;
            foreach ( $explode_key as $load ) {
                $newKey .= '-' . $load;
            }
        }
        return [ $newKey, $user, $company ];
    }
    /**
     * @param array $keys
     * @return array
     */
    protected function getCacheByArrayKeys( array $keys )
    {
        $response = [];
        foreach ( $keys as $key ) {
            [ $newKey, $user, $company ] = $this->keyPatternTransform( $key );
            if($newKey!==null){
                $data       = $this->getCache( $newKey, $user, $company );
                $response[] = $data;
            }
        }
        return $response;
    }
    protected function deleteCacheByArrayKeys( array $keys )
    {
        foreach ( $keys as $key ) {
            [ $newKey, $user, $company ] = $this->keyPatternTransform( $key );
            if($newKey!==null){
                 $this->forgetCache( $newKey, $user, $company );
            }
        }
    }

    protected function setCacheByKeyHandMade( $user, $company, $model, $keys = [], $data = [], $time = 30 )
    {
        $key = "{$user->id}-{$company->id_company}-{$model}";
        foreach ( $keys as $load){
            $key = $load === '' ? $key : $key . '-' . $load;
        }
        $this->cache( $key, $data, $time, $user->id, $company->id_company );
    }

    protected function getCacheByKeyHandMade( $user, $company, $model, $keys = [] )
    {
        $key = "{$user->id}-{$company->id_company}-{$model}";
        foreach ( $keys as $load){
            $key = $load === '' ? $key : $key . '-' . $load;
        }
        return $this->getCache( $key, $user->id, $company->id_company );
    }

    protected function getForgetCacheByKeyHandMade( $user, $company, $model, $keys = [] )
    {
        $key = "{$user->id}-{$company->id_company}-{$model}";
        foreach ( $keys as $load){
            $key = $load === '' ? $key : $key . '-' . $load;
        }
        $this->forgetCache( $key, $user, $company );
    }

    function mapKeyCache( $company, $user, $model, $key, $time = 30 ): callable
    {
        return function ( $item ) use ( $company, $user, $model, $key, $time ) {
            $item_key = $item[ $key ];
            $this->setCacheByKeyHandMade( $user, $company, $model, [ $item_key ], $item, $time  );
            return $item;
        };
    }

    function mapForgetKeyCache( $company, $user, $model, $key ): callable
    {
        return function ( $item ) use ( $company, $user, $model, $key ) {
            $item_key = $item[ $key ];
            $this->getForgetCacheByKeyHandMade( $user, $company, $model, [ $item_key ] );
            return $item;
        };
    }

    function mapKeysArrayCache( $company, $user, $model, array $keys, $time = 30 ): callable
    {
        return function ( $item ) use ( $company, $user, $model, $keys, $time ) {
            $newsKeys = [];
            foreach ($keys as $key){
                    $newsKeys[] = $item[$key];
            }

            $this->setCacheByKeyHandMade( $user, $company, $model, $newsKeys, $item, $time );
            return $item;
        };
    }

    function mapForgetKeysArrayCache( $company, $user, $model, array $keys ): callable
    {
        return function ( $item ) use ( $company, $user, $model, $keys ) {
            $this->getForgetCacheByKeyHandMade( $user, $company, $model, $keys );
            return $item;
        };
    }

    protected function getForgetCacheByKeyPattern( $user, $company, $keyPattern )
    {
        $user_company = "laravel:{$user->id}_{$company->id_company}";
        $key          = $user_company . "_".$keyPattern;
        $array_keys   = $this->getKeysByPattern( $key );
        $this->deleteCacheByArrayKeys( $array_keys );
    }
}
