<?php

namespace App\Console\Commands;

use App\Core\Notes\Services\NoteService;
use Illuminate\Console\Command;

/**
 * Class SendNotificationStoreNoteEveryWeekCommand
 * @package App\Console\Commands
 */
class SendNotificationStoreNoteEveryWeekCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:notification_store_note_every_week';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notification store note every week';

    /**
     * @var NoteService
     */
    private $noteService;

    /**
     * sendNotificationStoreNoteEveryWeek constructor.
     * @param NoteService $noteService
     */
    public function __construct( NoteService $noteService)
    {
        parent::__construct();
        $this->noteService = $noteService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->noteService->lookingNoteCreateByWeek();
        //
    }
}
