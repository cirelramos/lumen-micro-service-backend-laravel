<?php

namespace App\Core\User\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Core\User\Models\OauthAccessToken
 *
 * @property bigint    $user_id    user id
 * @property int       $client_id  client id
 * @property varchar   $name       name
 * @property text      $scopes     scopes
 * @property tinyint   $revoked    revoked
 * @property timestamp $created_at created at
 * @property timestamp $updated_at updated at
 * @property datetime  $expires_at expires at
 * @property int $id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereExpiresAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereRevoked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereScopes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Core\User\Models\OauthAccessToken whereUserId($value)
 * @mixin \Eloquent
 */
class OauthAccessToken extends Model
{

    /**
     * Database table name
     */
    protected $table = 'oauth_access_tokens';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'expires_at',
        'user_id',
        'client_id',
        'name',
        'scopes',
        'revoked',
        'expires_at',
    ];

    /**
     * Date time columns.
     */
    protected $dates = [ 'expires_at' ];

}
