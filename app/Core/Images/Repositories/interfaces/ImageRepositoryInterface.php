<?php

namespace App\Core\Images\Repositories\interfaces;

use App\Core\Images\Models\Image;
use App\Core\Notes\Models\Note;
use App\Core\Notes\Models\NotesHasImage;

/**
 * Interface ImageRepositoryInterface
 * @package App\Core\Images\Repositories\interfaces
 */
interface ImageRepositoryInterface
{

    /**
     * @return Image
     */
    public function getModel(): Image;

    /**
     * @param      $arrayIdImage
     * @param Note $note
     * @return mixed
     */
    public function getNoteHasImagesByArrayImageAndNoteQuery( $arrayIdImage, Note $note );

    /**
     * @return NotesHasImage
     */
    public function getModelNoteHasImage(): NotesHasImage;

    /**
     * @param Image $image
     * @return mixed
     */
    public function save( Image $image );

    /**
     * @param $arrayIdImage
     * @return mixed
     */
    public function deleteImageByArrayImage( $arrayIdImage );
}
