<?php

namespace App\Core\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AssociateUserGroupRequest
 * @package App\Core\User\Requests
 */
class AssociateUserGroupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {

        return [ 'id_user' => 'required|numeric|exists:users,id', ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {

        return [

        ];
    }
}

