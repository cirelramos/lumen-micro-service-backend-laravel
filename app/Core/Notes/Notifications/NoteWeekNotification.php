<?php

namespace App\Core\Notes\Notifications;

use App\Traits\FileStorageS3;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class NoteStoreNotification
 * @package App\Core\Notes\Notification
 */
class NoteWeekNotification extends Notification implements ShouldQueue
{

    use FileStorageS3;
    use Queueable;

    /**
     * @var
     */
    private $via;

    /**
     * @var
     */
    private $notes;

    /**
     * NoteStoreNotification constructor.
     * @param $via
     */
    public function __construct( $notes)
    {

        $this->via   = [ 'mail' ];
        $this->notes = $notes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via( $notifiable )
    {

        return ( $this->via ) ? [ 'mail' ] : [ 'mail', 'broadcast' ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail( $notifiable ): MailMessage
    {

        return ( new MailMessage() )->subject( 'Notas Creadas esta Semana' )
            ->markdown( 'Notification.Resource.Notes.weekNoteNotificationTemplate', [
                'user' => $notifiable,
                'notes' => $this->notes,
            ] )
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray( $notifiable ): array
    {

        return [
            'message' => 'han creado una nota para ti',
            'from'    => '',
            'to'      => $notifiable->full_name,
            'image'   => '',
            'link'    => '',
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param mixed $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast( $notifiable ): BroadcastMessage
    {

    }

}

