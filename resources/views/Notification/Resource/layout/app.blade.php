<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=400px, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
</head>
<body
    style="background-color: #FFFFFF;color: #555151;height: 100%;hyphens: auto;line-height: 1.4;margin: 0;width: 100% !important;box-sizing: border-box;">

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0"
       style="margin: 0;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100% ;box-sizing: border-box; min-width: 850px">
    <tr>
        <td align='center' valign='top'
            style='border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;padding: 30px 0;background-color: rgb(245,248,250);'>
            <table class="content" width="100%" cellpadding="0" cellspacing="0"
                   style="margin: 0; padding: 0 5%;width: inherit; min-width: 900px;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%;box-sizing: border-box;">
                <tr>
                </tr>

                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0"
                        style="background-color: #FFFFFF;border-bottom: 1px solid #EDEFF2;margin: 0;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 100%;font-family: 'Lato', sans-serif;box-sizing: border-box;">
                        <table class="inner-body" align="center" cellpadding="0" cellspacing="0"
                               style="min-width: 600px;background-color: #FFFFFF;margin: 0 auto;padding: 0;width: 100%;-premailer-cellpadding: 0;-premailer-cellspacing: 0;-premailer-width: 570px;font-family: 'Lato', sans-serif;box-sizing: border-box;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell"
                                    style="padding: 10px 5px 35px 5px;font-family: 'Lato', sans-serif;box-sizing: border-box; ">
                                    @yield('content')
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                </tr>

            </table>
        </td>
    </tr>
</table>
</body>
</html>
